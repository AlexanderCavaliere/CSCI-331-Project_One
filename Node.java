/**
 * Node.java
 */

import java.util.ArrayList;
import java.util.HashMap;

public class Node
{
    private String name;
    private int cost;
    private int heuristic;
    private HashMap<String,Integer> neighbors;
    private Node parent;
    private boolean seen;

    public Node( String name, Integer cost )
    {
        this.name = name;
        this.cost = cost;
        this.heuristic = -1;

        neighbors = new HashMap<String,Integer>();
        seen = false;
    }

    public Node getParent()
    {
        return this.parent;
    }

    public void setParent( Node parent )
    {
        this.parent = parent;
    }

    public void setSeen()
    {
        seen = true;
    }

    public boolean getSeen()
    {
        return this.seen;
    }

    public void addNeighbor( String name, Integer cost )
    {
        neighbors.put( name, cost );
    }

    public HashMap<String,Integer> getNeighbors()
    {
        return this.neighbors;
    }
    
    public String getName()
    {
        return this.name;
    }

    public Integer getCost()
    {
        return this.cost;
    }

    public void setCost( Integer cost )
    {
        this.cost = cost;
    }
    
    public Integer getHeuristic()
    {
    	return this.heuristic;
    }
    
    public void setHeuristic( Integer heuristic )
    {
    	this.heuristic = heuristic;
    }
}
