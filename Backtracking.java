/**
 * Backtracking.java
 * @date 2017-03-18
 * @author Alexander R. Cavaliere <arc6393@rit.edu>
 */

import java.util.Queue;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.HashMap;

import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;

public class Backtracking
{
    public static void main( String[] args )
    {
        File file = new File( args[0] );
        BufferedReader reader = null;
        ArrayList<Node> graph = new ArrayList<Node>();
        ArrayList<String> fileLines = new ArrayList<String>();
        try
            {
                reader = new BufferedReader( new FileReader( file ) );
                String line = null;
                while( ( line = reader.readLine() ) != null )
                    {
                        fileLines.add( line );
                        String[] splitLine = line.split( "\\W" );
                        graph.add( new Node( splitLine[0], -1 ) );
                    }
                reader.close();
            }
        catch( IOException ioe )
            {
            }
        // Update each node's neighbor list
        try
            {
                for( String line : fileLines )
                    {
                        String[] splitLine = line.split( "\\W" );
                        String name = splitLine[0];
                        for( int pos = 1; pos < splitLine.length; pos += 2 )
                            {
                                for( Node node : graph )
                                    {
                                        if( name.equals( node.getName() ) )
                                            {
                                                node.addNeighbor(  splitLine[pos], new Integer( splitLine[pos+1] ) );
                                            }
                                    }
                            }
                    }
            }
        catch( Exception e )
            {
            }
        Scanner sc = new Scanner( System.in );

        System.out.printf( "Enter Name of Start Node: " );
        String startName = sc.next();
        System.out.printf( "Enter Name of End Node: " );
        String endName = sc.next();

        Node start = null;
        Node end = null;

        for( Node node : graph )
            {
                if( node.getName().equals( startName ) )
                    start = node;
                if( node.getName().equals( endName ) )
                    end = node;
                if( start != null && end != null )
                    break;
            }
        BacktrackingSearch bts = new BacktrackingSearch( start, end, graph );
        bts.search( start );
        bts.displayPath();
    }
} // End Backtracking

class BacktrackingSearch
{
    private Node start;
    private Node end;
    private ArrayList<Node> graph;

    public BacktrackingSearch( Node start, Node end, ArrayList<Node> graph )
    {
        this.start = start;
        this.end = end;
        this.graph = graph;
    }

    public void search( Node node )
    {
        node.setSeen();
        if( node.getName().equals( end.getName() ) )
            return;
        node.getNeighbors().forEach( (name, cost) ->
                                     {
                                         for( Node potential : graph )
                                             {
                                                 if( name.equals( potential.getName() ) && !potential.getSeen() )
                                                     {
                                                         potential.setParent( node );
                                                         potential.setCost( cost + node.getCost() );
                                                         search( potential );
                                                     }
                                             }
                                     });
    }

    public void displayPath()
    {
        System.out.printf( "Path cost is %d\n", end.getCost() );
        System.out.printf( "Path (End to Start): ");
        Node node = end;
        while( node.getParent() != null )
            {
                System.out.printf( "%s ", node.getName() );
                node = node.getParent();
            }
        System.out.printf( "%s\n", start.getName() );
    }
}


