/**
 * AStar.java
 * @date 2017-03-27
 * @author Tyler M. Quesnel <tmq4557@rit.edu>
 */

import java.util.Queue;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.HashMap;

import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;

public class AStar
{
    public static void main( String[] args )
    {
        File file = new File( args[0] );
        BufferedReader reader = null;
        ArrayList<Node> graph = new ArrayList<Node>();
        ArrayList<String> fileLines = new ArrayList<String>();
        try
        {
            reader = new BufferedReader( new FileReader( file ) );
            String line = null;
            while( ( line = reader.readLine() ) != null )
            {
                fileLines.add( line );
                String[] splitLine = line.split( "\\W" );
                Node newNode = new Node( splitLine[0], -1 );
                graph.add( newNode );
                newNode.setHeuristic(Integer.parseInt(splitLine[1]));
            }
            reader.close();
        }
        catch( IOException ioe )
        {
        }
        
        // Update each node's neighbor list
        try
        {
            for( String line : fileLines )
            {
                String[] splitLine = line.split( "\\W" );
                String name = splitLine[0];
                // unlike the other algorithms, A* includes a heuristic value after the name
                // so pos starts at 2 instead of 1.
                for( int pos = 2; pos < splitLine.length; pos += 2 )
                {
                    for( Node node : graph )
                    {
                        if( name.equals( node.getName() ) )
                        {
                            node.addNeighbor(  splitLine[pos], new Integer( splitLine[pos+1] ) );
                        }
                    }
                }
            }
        }
        catch( Exception e )
        {
        }
        Scanner sc = new Scanner( System.in );

        System.out.printf( "Enter Name of Start Node: " );
        String startName = sc.next();
        // Remove since all heuristics are built around GOL?
        System.out.printf( "Enter Name of End Node: " );
        String endName = sc.next();

        Node start = null;
        Node end = null;

        for( Node node : graph )
        {
            if( node.getName().equals( startName ) )
                start = node;
            if( node.getName().equals( endName ) )
                end = node;
            if( start != null && end != null )
                break;
        }
        AStarSearch astar = new AStarSearch( start, end, graph );
        astar.search( start, new ArrayList<Node>(), -1 );
        astar.displayPath();
    }
} // End AStar

class AStarSearch
{
    private Node start;
    private Node end;
    private ArrayList<Node> graph;

    public AStarSearch( Node start, Node end, ArrayList<Node> graph )
    {
        this.start = start;
        this.end = end;
        this.graph = graph;
    }

    /**
     * Performs an A* search on the data
     * 
     * @param node the node currently being processed
     * @param found the list of nodes that may be processed next (initially empty)
     * @param finalCost the current best value for reaching the goal node.  -1 before found
     */
    public void search( Node node, ArrayList<Node> found, int finalCost)
    {    	
    	node.setSeen();
    	// should only run for the first node, after that they are set throughout the function
    	if(node.getCost() == -1)
    		node.setCost(0);
    	if(found.contains(node))
    		found.remove(node);
    	if(node.getName().equals(end.getName()))
    	{
    		if(finalCost == -1 || finalCost > costOf(node))
    			finalCost = costOf(node);
    	}
    	else
    	{
    		updatePath(node);
	    	node.getNeighbors().forEach((name, cost) ->
	    	{
	    		
	    		for( Node potential : graph )
	            {
	                if( name.equals( potential.getName() ) && !potential.getSeen() )
	                {
	                	potential.setSeen();
	                	potential.setParent(node);
	                    potential.setCost(cost);
	                    found.add(potential);
	                }
	            }
	    	});
    	}
    	// Determine what node to search next by looking at the cost & heuristic
    	int minHG = Integer.MAX_VALUE;
    	Node next = null;
    	for( Node candidateNode : found )
    	{
    		if(costOf(candidateNode) + candidateNode.getHeuristic() < minHG)
    			next = candidateNode;
    	}
    	if(next != null)
    	{
    		for( Node tempfix : graph )
        		if(next.getName().equals(tempfix.getName()))
        			next = tempfix;
    		if(costOf(next) < finalCost || finalCost == -1)
    			search(next, found, finalCost);
    	}
    }
    
    public int costOf(Node node)
    {
    	int cost = node.getCost();
    	if(node.getParent() != null)
    		return cost+costOf(node.getParent());
    	return cost;
    }
    
    /**
     * Updates the path of nodes by going through the neighbors of a node, and
     * changing their parent if necessary
     * 
     * @param node the node to start updating the path from
     * @param cost the cost to compare to the value returned by costOf on the neighbors
     */
    public void updatePath(Node node)
    {
    	if(node.getSeen())
    	{
    		node.getNeighbors().forEach((name, cost) ->
    		{
    			for(Node potential : graph)
    			{
    				if(name.equals(potential.getName()) 
    						&& potential.getSeen() 
    						&& costOf(node) + cost < costOf(potential))
    				{
    					potential.setParent(node);
    					potential.setCost(cost);
    					updatePath(potential);
    				}
    			}
    		});
    	}
    }

    public void displayPath()
    {
        System.out.printf( "Path cost is %d\n", costOf(end) );
        System.out.printf( "Path (End to Start): ");
        Node node = end;
        while( node.getParent() != null )
        {
            System.out.printf( "%s ", node.getName() );
            node = node.getParent();
        }
        System.out.printf( "%s\n", start.getName() );
    }
}
/*
Enter Name of Start Node: GOS
Enter Name of End Node: GOL
Path cost is 215
Path (End to Start): GOL ENG INS CBT CAR SAN VIG UNI BOO LBR WAL GOS

 */

