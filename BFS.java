/**
 * BFS.java
 */

import java.util.Queue;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.HashMap;

import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;

public class BFS
{
    public static void main( String[] args )
    {
        File file = new File( args[0] );
        BufferedReader reader = null;
        ArrayList<Node> graph = new ArrayList<Node>();
        ArrayList<String> fileLines = new ArrayList<String>();
        
        try
            {
                reader = new BufferedReader( new FileReader( file ) );
                String line = null;
                while( ( line = reader.readLine() ) != null )
                    {
                        fileLines.add( line );
                        String[] splitLine = line.split( "\\W" );
                        graph.add( new Node( splitLine[0], -1 ) );
                    }
                reader.close();
            }
        catch( IOException ioe )
            {
            }
        // Update each node's neighbor list
        try
            {
                for( String line : fileLines )
                    {
                        String[] splitLine = line.split( "\\W" );
                        String name = splitLine[0];
                        for( int pos = 1; pos < splitLine.length; pos += 2 )
                            {
                                //System.out.printf( "%s ", splitLine[pos] );
                                for( Node node : graph )
                                    {
                                        if( name.equals( node.getName() ) )
                                            {
                                                node.addNeighbor(  splitLine[pos], new Integer( splitLine[pos+1] ) );
                                            }
                                    }
                            }
                        //System.out.println();
                    }
            }
        catch( Exception e )
            {
            }
        Scanner sc = new Scanner( System.in );

        System.out.printf( "Enter Name of Start Node: " );
        String startName = sc.next();
        System.out.printf( "Enter Name of End Node: " );
        String endName = sc.next();
        
        Node start = null;
        Node end = null;
        
        for( Node node : graph )
            {
                if( node.getName().equals( startName ) )
                    start = node;
                if( node.getName().equals( endName ) )
                    end = node;
                if( start != null && end != null )
                    break;
            }
        /*
        for( Node node : graph )
            {
                System.out.println();
                System.out.println( node.getName() );
                for( Node neighbor : node.getNeighbors() )
                    System.out.printf( "%s:%d ", neighbor.getName(), neighbor.getCost() );
                System.out.println();
            }
        */
        BreadthFirstSearch bfs = new BreadthFirstSearch( start, end, graph );
        bfs.Search();
    }
}

class BreadthFirstSearch 
{
    // Private Class State
    Node start;
    Node end;
    ArrayList<Node> graph;
    
    public BreadthFirstSearch( Node start, Node end, ArrayList<Node> graph )
    {
        this.start = start;
        this.end = end;
        this.graph = graph;
    }        

    public void Search()
    {
        start.setCost( 0 );
        ArrayList<Node> set = new ArrayList<Node>();
        Queue<Node> queue = new LinkedList<Node>();

        set.add( start );
        queue.add( start );

        while( !queue.isEmpty() )
            {
                Node current = queue.remove();
                if( current.getName().equals( end.getName() ) )
                    {
                        //end = current;
                        break;
                    }
                HashMap<String,Integer> neighbors = current.getNeighbors();
                neighbors.forEach( (name,cost) ->
                                   {
                                       boolean found = false;
                                       for( Node visited : set )
                                           if( name.equals( visited.getName() ) )
                                               {
                                                   found = true;
                                                   break;
                                               }
                                       if( !found )
                                           {
                                               Node gettingAdded = null;
                                               for( Node node : graph )
                                                   {
                                                       if( name.equals( node.getName() ) )
                                                           {
                                                               gettingAdded = node;
                                                               break;
                                                           }
                                                   }
                                               gettingAdded.setCost( current.getCost() + cost );
                                               set.add( gettingAdded );
                                               gettingAdded.setParent( current );
                                               queue.add( gettingAdded );
                                           }
                                   });
            }
        Node goal = end;

        System.out.printf( "Cost of the Path is: %d ", goal.getCost() );
        ArrayList<Node> path = new ArrayList<Node>();
        while( goal.getParent() != null )
            {
                path.add( goal );
                goal = goal.getParent();
            }
        path.add( goal );
        
        System.out.println();
        for( int position = path.size() - 1; position >= 0; --position )
            System.out.printf( "%s ", path.get( position ).getName() );
        System.out.println();
    }
}
